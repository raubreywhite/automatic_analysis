* Extracts effect estimates from analysis
capture program drop extract_estimates
program extract_estimates, rclass
	syntax anything, exposure(string)
	local anything=subinstr("`anything'","`exposure'","",.)

	quietly `anything' `exposure'
	local est=_b[`exposure']
	local se=_se[`exposure']
	
	return scalar est = `est'
	return scalar se = `se'
end

* Observes VIF for each confounder
capture program drop analysis_vif_one_confounder
program analysis_vif_one_confounder, rclass
	syntax anything, exposure(string) outcome(string) method(string)
	
	preserve
	
	quietly gen _outcome=""
	quietly gen _exposure=""
	quietly gen _confounder=""
	quietly gen _largest_vif=""
	quietly gen _vif=.
	
	* Obtains full estimates
	quietly `method' `outcome' `anything' `exposure'
	local orig_est=r(est)
	quietly estat vif
	
	local Y=1
	local N= e(df_m)
	forvalues Y=1/`N' {
		quietly replace _outcome="`outcome'" in `Y'
		quietly replace _exposure="`exposure'" in `Y'
		quietly replace _confounder=r(name_`Y') in `Y'
		quietly replace _vif=r(vif_`Y') in `Y'
	}

	quietly drop if _exposure==_confounder
	* Formats and stores results in "_minus_one_temp.dta"
	quietly keep _outcome-_vif
	quietly drop if missing(_outcome)
	quietly egen _temp=max(abs(_vif))
	quietly replace _largest_vif ="*" if float(abs(_temp))<=float(abs(_vif))+0.001 & float(abs(_temp))>=float(abs(_vif))-0.001 & abs(_temp)>5

	quietly drop _temp
	quietly save "_vif_temp.dta", replace
	restore 
end

* Observes change in beta coefficient by dropping one confounder
capture program drop analysis_minus_one_confounder
program analysis_minus_one_confounder, rclass
	syntax anything, exposure(string) outcome(string) method(string)
	
	preserve
	
	quietly gen _outcome=""
	quietly gen _exposure=""
	quietly gen _removed_confounder=""
	quietly gen _smallest_change=""
	quietly gen _perc_change=.
	quietly gen _est=.
	quietly gen _sd=.
	quietly gen _confounders=""
	
	* Obtains full estimates
	extract_estimates `method' `outcome' `anything', exposure("`exposure'")
	local orig_est=r(est)
	
	local Y=1
	foreach X of local anything {
		* Removes excess white space		
		local confounders_minus_one=subinstr("`anything'","`X'","",.)
		local confounders_minus_one=trim("`confounders_minus_one'")
		local confounders_minus_one=itrim("`confounders_minus_one'")
		
		* Obtains estimates minus one confounder
		extract_estimates `method' `outcome' `confounders_minus_one', exposure("`exposure'")
		
		quietly replace _outcome="`outcome'" in `Y'
		quietly replace _exposure="`exposure'" in `Y'
		quietly replace _confounders="`confounders_minus_one'" in `Y'
		quietly replace _removed_confounder="`X'" in `Y'
		quietly replace _perc_change=(r(est)-`orig_est')/`orig_est'*100 in `Y'
		quietly replace _est=r(est) in `Y'
		quietly replace _sd=r(se) in `Y'
		
		local Y=`Y'+1
	}
	
	* Formats and stores results in "_minus_one_temp.dta"
	quietly keep _outcome-_confounders
	quietly drop if missing(_outcome)
	quietly egen _temp=min(abs(_perc_change))
	quietly replace _smallest_change="*" if float(abs(_temp))<=float(abs(_perc_change))+0.001 & float(abs(_temp))>=float(abs(_perc_change))-0.001 & abs(_temp)<10
	quietly replace _confounders=subinstr(_confounders," ",", ",.)
	quietly replace _confounders=trim(_confounders)
	quietly replace _confounders=itrim(_confounders)
	quietly replace _confounders=reverse(subinstr(reverse(_confounders)," ,","",1)) if(regexm(_confounders,", $"))

	quietly drop _temp
	quietly save "_minus_one_temp.dta", replace
	restore 
end

* Saves diagnostic information into stata files "_diagnostics.dta" and "_diagnostics_explanatory_variables.dta"
capture program drop add_diagnostics
program add_diagnostics, rclass
	syntax [anything], exposure(string) outcome(string) method(string) strata(string)
	
	preserve
	
	local anything=subinstr("`anything'","`exposure'","",.)
	local anything=subinstr("`anything'","`outcome'","",.)
	local anything=subinstr("`anything'","`method'","",.)

	* Restricting dataset to complete case analysis
	quietly `method' `outcome' `exposure' `anything'
	quietly keep if e(sample)==1
	
	tempfile orig_data
	save `orig_data'
	
	******************************
	* DIAGNOSTICS FOR RESIDUALS
	if("`method'"=="logit"){
		quietly predict _predicted, pr
		quietly ren `outcome' _observed
		quietly keep _predicted _observed
		quietly gen blah=runiform()
		quietly sort _predicted blah
		quietly count
		local N=r(N)
		if(`N'>=100){
			* Bin the observations and generate standardized Pearson residuals
			local bin_size=round(`N'/200)
			if(`bin_size'<10){
				local bin_size=10
			}
			quietly gen bin=floor(_n/`bin_size')
			quietly collapse (mean) _observed _predicted, by(bin)
			quietly gen _resid= _observed-_predicted
			quietly sum _resid
			quietly replace _resid=_resid/r(sd)
			quietly gen _type="binned"
			quietly drop bin
		}
		else {
			* Keep the observations as raw
			quietly gen _type="raw"
			quietly drop blah
		}
	}
	else if("`method'"=="regress"){
		* Generate studentized residuals
		quietly predict _predicted, xb
		quietly predict _resid, rstudent
		quietly ren `outcome' _observed
		quietly keep _predicted _observed _resid
		quietly gen _type="raw"
	}
	
	quietly gen _strata="`strata'"
	quietly gen _exposure="`exposure'"
	quietly gen _outcome="`outcome'"
	quietly gen _method="`method'"
	
	* If there are already diagnostics saved, append current results
	quietly capture confirm file "_diagnostics.dta"
	if(_rc==0){
		tempfile temp
		quietly save `temp'
		quietly use "_diagnostics.dta", clear
		quietly append using `temp'
	}
	quietly save "_diagnostics.dta", replace

	******************************
	* RESIDUALS VS CONFOUNDERS/EXPLANATORY VARIABLES	
	
	quietly use `orig_data', clear
	foreach X of varlist `exposure' `anything' {
		quietly gen _c_`X'=`X' 
	}
	if("`method'"=="logit"){
		quietly predict _predicted, pr
		quietly ren `outcome' _observed
		quietly keep _predicted _observed _c_*
		quietly gen blah=runiform()
		quietly sort _predicted blah
		quietly count
		local N=r(N)
		if(`N'>=100){
			* Bin the observations and generate standardized Pearson residuals
			local bin_size=round(`N'/200)
			if(`bin_size'<10){
				local bin_size=10
			}
			quietly gen bin=floor(_n/`bin_size')
			quietly collapse (mean) _observed _predicted _c_*, by(bin)
			quietly gen _resid= _observed-_predicted
			quietly sum _resid
			quietly replace _resid=_resid/r(sd)
			quietly gen _type="binned"
			quietly drop bin
		}
		else {
			* Keep the observations as raw
			quietly gen _type="raw"
			quietly drop blah
		}
	}
	else if("`method'"=="regress"){
		* Generate studentized residuals
		quietly predict _predicted, xb
		quietly predict _resid, rstudent
		quietly ren `outcome' _observed
		quietly keep _predicted _observed _resid _c_*
		quietly gen _type="raw"
	}
	
	quietly gen _strata="`strata'"
	quietly gen _exposure="`exposure'"
	quietly gen _outcome="`outcome'"
	quietly gen _method="`method'"
	
	* If there are already results saved, append current results
	quietly capture confirm file "_diagnostics_explanatory_variables.dta"
	if(_rc==0){
		quietly save `temp', replace
		quietly use "_diagnostics_explanatory_variables.dta", clear
		quietly append using `temp'
	}
	quietly save "_diagnostics_explanatory_variables.dta", replace
	
	restore
end

* Graphs diagnostics
capture program drop graph_diagnostics
program graph_diagnostics, rclass
	syntax anything
	
	preserve
	quietly !"$LOCATION_OF_R" $R_EXTRA_COMMAND "$PROGRAM_DIRECTORY/Libraries/0_plot_diagnostics.R" "`c(pwd)'" "$RESULTS_DIRECTORY/`anything'"
	capture erase "_diagnostics_explanatory_variables.csv"
	capture erase "_diagnostics.csv"
	restore
end

* Saves information on crude relationship between outcome and exposure to stata file "_crude_outcome_exposure.dta"
capture program drop add_crude_outcome_exposure
program add_crude_outcome_exposure, rclass
	syntax [anything], exposure(string) outcome(string) method(string) strata(string)
	
	preserve
	
	local anything=subinstr("`anything'","`exposure'","",.)
	local anything=subinstr("`anything'","`outcome'","",.)
	local anything=subinstr("`anything'","`method'","",.)

	* Restricting dataset to complete case analysis
	quietly `method' `outcome' `exposure'
	quietly keep if e(sample)==1
	
	quietly gen _c_exposure=`exposure'
	quietly count if !missing(_c_exposure) & _c_exposure!=0 & _c_exposure!=1
	quietly ren `outcome' _observed
	quietly keep _observed _c_exposure
	if(r(N)==0){
		* If the exposure is binary, then just take mean and 95% CI of outcome
		quietly gen _sd=_observed
		quietly gen n=1
		quietly collapse (mean) _observed (sd) _sd (sum) n, by(_c_exposure)
		quietly replace _sd=_sd/sqrt(n)
		quietly gen _l95=_observed-1.96*_sd
		quietly gen _u95=_observed+1.96*_sd
		quietly drop n _sd
		quietly gen _type="categorical"
	}
	else if("`method'"=="logit"){
		* Exposure is continuous
		quietly count
		local N=r(N)
		if(`N'>=100){
			* Bin the observations (will display using linear regression)
			local bin_size=round(`N'/200)
			if(`bin_size'<10){
				local bin_size=10
			}
			quietly sort _c_exposure
			quietly gen bin=floor(_n/`bin_size')

			quietly collapse (mean) _observed _c_exposure, by(bin)
			quietly gen _type="binned"
			quietly drop bin
		}
		else {
			* Keep the observations as raw (will display using GLMs)
			quietly gen _type="logit_continuous_exposure"
		}
	}
	else {
		* Keep the observations as raw (will display using linear regression)
		quietly gen _type="continuous_exposure"
	}
	
	quietly gen _strata="`strata'"
	quietly gen _exposure="`exposure'"
	quietly gen _outcome="`outcome'"
	quietly gen _method="`method'"
	
	* If there are already outcome vs exposure results saved, append current results
	quietly capture confirm file "_crude_outcome_exposure.dta"
	if(_rc==0){
		tempfile temp
		quietly save `temp'
		quietly use "_crude_outcome_exposure.dta", clear
		quietly append using `temp'
	}
	quietly save "_crude_outcome_exposure.dta", replace

	restore
end

* Graphs crude relationship between outcome and exposure
capture program drop graph_crude_outcome_exposure
program graph_crude_outcome_exposure, rclass
	syntax anything
	
	preserve
	quietly !"$LOCATION_OF_R" $R_EXTRA_COMMAND "$PROGRAM_DIRECTORY/Libraries/0_plot_outcome_vs_exposure.R" "`c(pwd)'" "$RESULTS_DIRECTORY/`anything'"
	capture erase "_crude_outcome_exposure.csv"
	*capture erase "_diagnostics.csv"
	restore
end
	
* Given an outcome, exposure, and confounders, this function selects the appropriate
* confounders (process stored in "_minus_one.dta") and then outputs the results for
* the reduced, complete, and crude models in "_analysis.dta"
capture program drop automated_analysis
program automated_analysis, rclass
	syntax anything, exposure(string) outcome(string) method(string) strata(string)
	
	* How many confounders are there?
	local num_conf=0
	foreach X of local anything {
		local num_conf=`num_conf'+1
	}

	* Determine which confounders are important to the analysis, using VIF
	local confounders="`anything'"
	if("`method'"=="regress"){
		forvalues X=1/`num_conf' {
			preserve
			analysis_vif_one_confounder `confounders', exposure("`exposure'") outcome("`outcome'") method("`method'")
			quietly use "_vif_temp.dta", clear
			quietly gen run=`X'
			if(`X'>1){
				quietly append using "_vif_temp.dta"
			}
			quietly save "_vif.dta", replace
			quietly use "_vif_temp.dta", clear
			capture erase "_vif_temp.dta"
			quietly keep if _largest=="*"
			quietly count
			local N=r(N)
			if(`N'==0){
				restore
				continue, break
			}
			local selection=_confounder[1]
			local confounders=subinstr("`confounders'","`selection'","",.)
			restore
		}
	}
	
	* Determine which confounders are important to the analysis, using change in beta estimate
*	local confounders="`anything'"
	forvalues X=1/`num_conf' {
		preserve
		analysis_minus_one_confounder `confounders', exposure("`exposure'") outcome("`outcome'") method("`method'")
		quietly use "_minus_one_temp.dta", clear
		quietly gen run=`X'
		if(`X'>1){
			quietly append using "_minus_one.dta"
		}
		quietly save "_minus_one.dta", replace
		quietly use "_minus_one_temp.dta", clear
		capture erase "_minus_one_temp.dta"
		quietly keep if _smallest=="*"
		quietly count
		local N=r(N)
		if(`N'==0){
			restore
			continue, break
		}
		local selection=_removed_confounder[1]
		local confounders=subinstr("`confounders'","`selection'","",.)
		restore
	}

	quietly gen _outcome=""
	quietly gen _exposure=""
	quietly gen _type=""
	quietly gen _est=.
	quietly gen _sd=.
	quietly gen _conf=""

	* Run crude, total, and reduced models and save the results
	forvalues RUN=1/3 {
		quietly replace _outcome="`outcome'" in `RUN'
		quietly replace _exposure="`exposure'" in `RUN'
		
		if(`RUN'==1){
			* crude
			local conf=""
			quietly replace _type="_crude" in `RUN'
		}
		else if(`RUN'==2){
			* everything
			local conf="`anything'"
			quietly replace _type="_everything" in `RUN'
		}
		else if(`RUN'==3){
			* final
			local conf="`confounders'"
			quietly replace _type="_final" in `RUN'
			
			* Obtain diagnostics and outcome v exposure results
			capture add_diagnostics `conf', exposure("`exposure'") outcome("`outcome'") method("`method'") strata("`strata'")
			capture add_crude_outcome_exposure `conf', exposure("`exposure'") outcome("`outcome'") method("`method'") strata("`strata'")
		}

		extract_estimates `method' `outcome' `conf', exposure("`exposure'")

		quietly replace _est=r(est) in `RUN'
		quietly replace _sd=r(se) in `RUN'
		quietly replace _conf="`conf'" in `RUN'
	}
	quietly keep _outcome-_conf
	quietly drop if missing(_outcome)
	
	quietly gen _l95 = _est-1.96*_sd
	quietly gen _u95 = _est+1.96*_sd
	quietly gen _pvalue = 2*(1-normal(abs(_est/_sd))) 
	
	* Exponentiate if required
	if("`method'"=="logit"){
		quietly replace _l95=exp(_l95)
		quietly replace _u95=exp(_u95)
		quietly replace _est=exp(_est)
	}
	
	* Cleaning up and formatting
	quietly gen sig=""
	quietly replace sig="+" if _pvalue<0.1
	quietly replace sig="*" if _pvalue<0.05
	quietly replace sig="**" if _pvalue<0.01
	quietly replace sig="***" if _pvalue<0.001
	quietly tostring _est, replace format("%12.2f") force
	quietly tostring _l95, replace format("%12.2f") force
	quietly tostring _u95, replace format("%12.2f") force
	quietly tostring _pvalue, replace format("%12.3f") force
	quietly gen _res=_est+" ("+_l95+", "+_u95+")"+sig
	quietly drop sig
	
	quietly keep _outcome _exposure _type _res _pvalue _conf
	
	quietly reshape wide _res _pvalue _conf, i(_outcome _exposure) j(_type) string
	
	quietly drop _conf_crude
	quietly order _outcome _exposure _res_final _res_everything _res_crude _pvalue_final _pvalue_everything _pvalue_crude _conf_final _conf_everything
	quietly save "_analysis.dta", replace	
	
	quietly use "_minus_one.dta", clear
	quietly order run
	quietly gen f=abs(_perc)
	quietly gsort run f
	quietly drop f
	quietly save "_minus_one.dta", replace
	
	quietly capture confirm file "_vif.dta"
	if(_rc==0){
		quietly use "_vif.dta", clear
		quietly order run
		quietly gsort run _vif
		quietly save "_vif.dta", replace
	}
end

capture program drop replace_variables_with_names
program replace_variables_with_names, rclass
	syntax anything using/, variables(string)

	quietly import excel using "`anything'", clear firstrow
	quietly count if !missing(variable_names)
	local N=r(N)
	local variable_names=""
	local variable_labels=""
	forvalues X=1/`N' {
		local variable_names=variable_names[`X']
		local variable_labels=variable_labels[`X']
		preserve
		quietly use `using', clear
		foreach Y of varlist `variables' {
			quietly replace `Y'=subinstr(`Y',"`variable_names'","`variable_labels'",.)
		}
		quietly save `using', replace
		restore
	}
end

capture program drop replace_strata_with_names
program replace_strata_with_names, rclass
	syntax anything using/, variables(string)
	
	quietly import excel using "`anything'", clear firstrow
	quietly count if !missing(strata_variables)
	local N=r(N)
	local strata_variables =""
	local strata_names=""
	forvalues X=1/`N' {
		local strata_variables = strata_variables[`X']
		local strata_names = strata_names[`X']
		preserve
		quietly use `using', clear
		foreach Y of varlist `variables' {
			quietly replace `Y'=subinstr(`Y',"`strata_variables'","`strata_names'",.)
		}
		quietly save `using', replace
		restore
	}
end

* Graphs crude relationship between outcome and exposure (main results)
capture program drop graph_main_results
program graph_main_results, rclass
	syntax anything
	
	quietly import excel using "`anything'", clear firstrow
	quietly keep outcome exposure figure_file_name include_strata_in_figure
	quietly drop if missing(figure_file_name)
	quietly duplicates drop
	quietly save "_to_plot_outcome_exposure.dta", replace

	quietly replace_variables_with_names `anything' using _to_plot_outcome_exposure.dta, variables("outcome exposure")

	quietly import excel using "`anything'", clear firstrow
	quietly keep strata_names
	local first_strata=strata_names[1]

	quietly use "_crude_outcome_exposure.dta", clear
	quietly gen figure_name=""
	quietly save "_crude_outcome_exposure_varying_strata.dta", replace

	quietly use "_to_plot_outcome_exposure.dta", clear
	quietly count
	local N=r(N)
	forvalues X=1/`N'{
		local exp=exposure[`X']
		local out=outcome[`X']
		local file=figure_file_name[`X']
		local all_strata=include_strata_in_figure[`X']
		preserve
		quietly use "_crude_outcome_exposure_varying_strata.dta", clear
		if("`all_strata'"=="yes"){
			quietly replace figure_name="`file'" if _exposure=="`exp'" & _outcome=="`out'"
		}
		else {
			quietly replace figure_name="`file'" if _exposure=="`exp'" & _outcome=="`out'" & _strata=="`first_strata'"
		}
		quietly save "_crude_outcome_exposure_varying_strata.dta", replace
		restore
	}

	quietly use "_crude_outcome_exposure_varying_strata.dta", clear
	quietly drop if missing(figure_name)
	quietly outsheet using "_crude_outcome_exposure_varying_strata.csv", comma replace
	
	preserve
	quietly !"$LOCATION_OF_R" $R_EXTRA_COMMAND "$PROGRAM_DIRECTORY/Libraries/0_plot_outcome_vs_exposure_varying_strata.R" "`c(pwd)'" "$RESULTS_DIRECTORY"
	capture erase "_to_plot_outcome_exposure.dta"
	capture erase "_crude_outcome_exposure.dta"
	capture erase "_crude_outcome_exposure_varying_strata.dta"
	capture erase "_crude_outcome_exposure_varying_strata.csv"
	*capture erase "_diagnostics.csv"
	restore
end

* Takes input from excel file and runs automated analyses according to the
* given specifications
capture program drop analyse_from_excel
program analyse_from_excel, rclass
	syntax [anything] using/
		
	* Checks to make sure all files are where they should be
	quietly capture confirm file "$RESULTS_DIRECTORY"
	if(_rc!=0){
		quietly capture mkdir "$RESULTS_DIRECTORY"
	}
	if("`anything'"==""){
		local anything="Analyses/2_analyses.xlsx"
	}
	
	quietly capture confirm file "`anything'"
	if(_rc!=0){
		error 601
	}
	quietly capture confirm file "`using'"
	if(_rc!=0){
		error 601
	}
	
	* Erase files that shouldn't be there
	local myfilelist: dir "." files "_*.dta"
	foreach filename of local myfilelist {
		capture erase "`filename'"
	}
	
	* Will this descriptive table require footnotes?
	quietly use "`using'", clear	
	quietly import excel using "`anything'", clear firstrow
	quietly keep footnotes
	quietly drop if missing(footnotes)
	quietly ren footnotes A
	tempfile footnotes
	quietly save `footnotes'
	local use_footnotes=0
	quietly count
	local N=r(N)
	if(`N'>0){
		local use_footnotes=1
	}

	* Reading in flags
	tempfile final_results supplemental vif
	quietly import excel using "`anything'", clear firstrow
	
	local file_name=file_name[1]
	local title=title[1]
	
	local supp_vif_file_name=supp_vif_file_name[1]
	local supp_vif_file_title=supp_vif_file_title[1]
	
	local supplemental_file_name=supp_beta_change_file_name[1]
	local supplemental_file_title=supp_beta_change_file_title[1]
	
	local diagnostics_file_name=diagnostics_file_name[1]
	local outcome_exp_figure_file_name=outcome_exp_figure_file_name[1]
	
	* Loading in strata variables and labels
	quietly count if !missing(strata_variables)
	local N=r(N)
	local strata=""
	local strata_labels=""
	forvalues X=1/`N' {
		local temp=strata_variables[`X']
		local strata="`strata' `temp'"
		
		local temp=subinstr(strata_names[`X']," ",".",.)
		local strata_labels="`strata_labels' `temp'"
	}
	
	* Running the automatic analyses
	quietly count if !missing(outcome)
	local total_analysis=r(N)*`N'
	local N=r(N)
	local Y=1
	local num_vif=0
	local last_X_val=0
	* For each strata
	foreach S of local strata {
		* For each analysis
		forvalues X=1/`N' {
			di "`Y'/`total_analysis'"
			preserve
			
			* Loading up the analysis
			local method=method[`X']
			local outcome=outcome[`X']
			local exposure=exposure[`X']
			local confounders=confounders[`X']
			
			* Load the data and select the strata
			quietly use "`using'", clear	
			quietly keep if `S'
			
			* Testing to see if the model will converge
			local extra=""
			if("`method'"=="logit"){
				local extra=", iter(50)"
			}
			quietly capture `method' `outcome' `exposure' `confounders' `extra'
			if(_rc!=0){
				local total_analysis=`total_analysis'-1
				restore
				di "-FAIL-: `method' `outcome' `exposure' if `S'"
				continue
			}
			if(e(converged)==0){
				local total_analysis=`total_analysis'-1
				restore
				di "-FAIL-"
				continue
			}
			
			* If model works, then send it off to be automated
			automated_analysis `confounders', exposure("`exposure'") outcome("`outcome'") method("`method'") strata("`S'")

			* Save results
			quietly use "_analysis.dta", clear
			quietly gen strata="`S'"
			if(`Y'!=1){
				quietly use `final_results', clear
				if(`X'<`last_X_val'){
					add_one_line
				}
				quietly append using "_analysis.dta"
				quietly replace strata="`S'" if missing(strata)
			}
			local last_X_val=`X'
			quietly save `final_results', replace
			
			* Save decisions used in creating final models using VIF
			if("`method'"=="regress"){
				quietly use "_vif.dta", clear
				quietly gen strata="`S'"
				if(`num_vif'!=0){
					quietly use `vif', clear
					add_one_line
					quietly append using "_vif.dta"
					quietly replace strata="`S'" if missing(strata)
				}
				local num_vif=`num_vif'+1
				quietly save `vif', replace	
			}
			
			* Save decisions used in creating final models using % change in beta coefficients
			quietly use "_minus_one.dta", clear
			quietly gen strata="`S'"
			if(`Y'!=1){
				quietly use `supplemental', clear
				add_one_line
				quietly append using "_minus_one.dta"
				quietly replace strata="`S'" if missing(strata)
			}
			quietly save `supplemental', replace
			restore
			local Y=`Y'+1
		}
	}
	
	* Cleaning up excess whitespace
	quietly use `final_results', clear
	quietly replace _conf_final=trim(_conf_final)
	quietly replace _conf_final=itrim(_conf_final)
	quietly replace _conf_final=subinstr(_conf_final," ",", ",.)
	quietly replace _conf_everything=trim(_conf_everything)
	quietly replace _conf_everything =itrim(_conf_everything)
	quietly replace _conf_everything =subinstr(_conf_everything," ",", ",.)
	quietly save "_analysis.dta", replace

	* Replacing variable names with pretty labels
	replace_variables_with_names `anything' using _analysis.dta, variables("_outcome _exposure _conf*")
	replace_strata_with_names `anything' using _analysis.dta, variables("strata")

	quietly use "_analysis.dta", clear
	quietly erase "_analysis.dta"
	quietly order strata

	quietly replace strata="" if missing(_outcome)
	foreach X of varlist _pvalue* {
		quietly replace `X'="<0.001" if `X'=="0.000"
	}
	label var strata "Strata"
	label var _outcome "Outcome"
	label var _exposure "Exposure"
	label var _res_final "Reduced model"
	label var _res_everything "Complete model"
	label var _res_crude "Crude model"
	label var _pvalue_final "Pvalue (Reduced)"
	label var _pvalue_everything "Pvalue (Complete)"
	label var _pvalue_crude "Pvalue (Crude)"
	label var _conf_final "Confounders included in reduced model"
	label var _conf_everything "Confounders included in complete model"

	quietly export excel using "$RESULTS_DIRECTORY/`file_name'", replace firstrow(varlabels)
	quietly import excel using "$RESULTS_DIRECTORY/`file_name'", clear

	* Adding in title
	shuffle_down
	shuffle_down
	quietly replace A="`title'" in 1
	
	* Adding the table footnotes
	if(`use_footnotes'==1){
		add_one_line
		quietly append using `footnotes'
	}
	
	* Final table produced
	quietly make_clean
	quietly export excel using "$RESULTS_DIRECTORY/`file_name'", replace

	**** SUPPLEMENTAL RESULTS DETAILING CONFOUNDER SELECTION USING VIF
	quietly capture confirm file "_vif.dta"
	if(_rc==0){
		quietly use `vif', clear
		quietly save "_vif.dta", replace

		* Replacing variable names with pretty labels
		replace_variables_with_names `anything' using _vif.dta, variables("_outcome _exposure _confounder")
		replace_strata_with_names `anything' using _vif.dta, variables("strata")

		quietly use "_vif.dta", clear
		quietly order strata
		label var strata "Strata"
		label var run "Run #"
		label var _outcome "Outcome"
		label var _exposure "Exposure"
		label var _confounder "Tested variable"
		label var _largest "Selected for removal"
		label var _vif "VIF"

		quietly tostring _vif, replace format("%12.2f") force
		quietly replace strata="" if missing(run)
		quietly tostring run, replace force
	
		make_clean

		quietly erase "_vif.dta"
	
		quietly export excel using "$RESULTS_DIRECTORY/`supp_vif_file_name'", replace firstrow(varlabels)
		quietly import excel using "$RESULTS_DIRECTORY/`supp_vif_file_name'", clear
	
		shuffle_down
		shuffle_down
	
		quietly replace A="`supp_vif_file_title'" in 1
		quietly export excel using "$RESULTS_DIRECTORY/`supp_vif_file_name'", replace
	}

	**** SUPPLEMENTAL RESULTS DETAILING CONFOUNDER SELECTION USING CHANGE IN BETA COEFFICIENT
	quietly use `supplemental', clear
	quietly save "_minus_one.dta", replace

	* Replacing variable names with pretty labels
	replace_variables_with_names `anything' using _minus_one.dta, variables("_outcome _exposure _removed _confounder")
	replace_strata_with_names `anything' using _minus_one.dta, variables("strata")

	quietly use "_minus_one.dta", clear
	quietly order strata
	label var strata "Strata"
	label var run "Run #"
	label var _outcome "Outcome"
	label var _exposure "Exposure"
	label var _removed_confounder "Tested variable (by removing)"
	label var _smallest "Selected for removal"
	label var _perc_change "Perc change"
	label var _est "Beta estimate"
	label var _sd "Std err"
	label var _conf "Remaining confounders"

	quietly tostring _perc_change, replace format("%12.1f") force
	quietly tostring _est, replace format("%12.3f") force
	quietly tostring _sd, replace format("%12.3f") force
	quietly replace strata="" if missing(run)
	quietly tostring run, replace force
	
	make_clean
	
	quietly erase "_minus_one.dta"
	
	quietly export excel using "$RESULTS_DIRECTORY/`supplemental_file_name'", replace firstrow(varlabels)
	quietly import excel using "$RESULTS_DIRECTORY/`supplemental_file_name'", clear
	
	shuffle_down
	shuffle_down
	
	quietly replace A="`supplemental_file_title'" in 1
	quietly export excel using "$RESULTS_DIRECTORY/`supplemental_file_name'", replace
	
	**********************************
	**********************************
	**********************************
	**********************************	
	**** DIAGNOSTICS
	di "Generating diagnostics..."
	replace_variables_with_names `anything' using _diagnostics.dta, variables("_outcome _exposure")	
	replace_strata_with_names `anything' using _diagnostics.dta, variables("_strata")

	quietly use "_diagnostics.dta", clear
	quietly outsheet using "_diagnostics.csv", comma replace
	capture erase "_diagnostics.dta"
	
	**** DIAGNOSTICS EXPLANATORY VARIABLES
	tempfile diagnostics_explanatory
	quietly use "_diagnostics_explanatory_variables.dta", clear
	quietly gen id=_n
	quietly reshape long _c_, i(id) j(explanatory_var) string
	quietly drop if missing(_c_)
	quietly save "_diagnostics_explanatory_variables.dta", replace

	* Replacing variable names with pretty labels	
	replace_variables_with_names `anything' using _diagnostics_explanatory_variables.dta, variables("_outcome _exposure explanatory_var")
	replace_strata_with_names `anything' using _diagnostics_explanatory_variables.dta, variables("_strata")

	quietly use "_diagnostics_explanatory_variables.dta", clear
	quietly outsheet using "_diagnostics_explanatory_variables.csv", comma replace
	capture erase "_diagnostics_explanatory_variables.dta"
	
	* Plotting diagnostics
	di "Plotting diagnostics..."
	graph_diagnostics `diagnostics_file_name'
	
	**** CRUDE OUTCOME VS EXPOSURE GRAPHS
	di "Generating outcome vs exposure data..."
	tempfile outcome_vs_exposure
	quietly use "_crude_outcome_exposure.dta", clear
	quietly save `outcome_vs_exposure'

	* Replacing variable names with pretty labels
	replace_variables_with_names `anything' using _crude_outcome_exposure.dta, variables("_outcome _exposure")	
	replace_strata_with_names `anything' using _crude_outcome_exposure.dta, variables("_strata")

	quietly use "_crude_outcome_exposure.dta", clear
	quietly outsheet using "_crude_outcome_exposure.csv", comma replace
*	capture erase "_crude_outcome_exposure.dta"

	* Plotting outcome vs exposure graphs
	di "Plotting outcome vs exposure graphs..."	
	graph_crude_outcome_exposure `outcome_exp_figure_file_name'

	* Plotting final main graphs for the paper
	graph_main_results `anything'

	di "--FINISHED--"
end



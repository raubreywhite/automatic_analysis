library(mgcv)
library(splines)
library(ggplot2)
library(grid)
arguments <- commandArgs(TRUE)
#arguments <- c()
#arguments[1] <- "/Volumes/Research/Code/MOBA breastfeeding"
#arguments[2] <- "Figure_S1"

setwd(arguments[1])
data <- read.csv("_crude_outcome_exposure_varying_strata.csv")
setwd(arguments[2])
size.multiplier <- 2

for(figure in unique(data$figure_name)){
	index <- data$figure_name==figure
	if(sum(index)==0) next
	plotting.data <- data[index,]
	
	to.plot.data <- list()
	to.plot.graphs <- list()
	num.graphs <- 0
	incorporate_strata <- FALSE
	if(length(unique(plotting.data$X_strata))>1) incorporate_strata <- TRUE
	for(strata in unique(plotting.data$X_strata)) for(outcome in unique(plotting.data$X_outcome)) for(exposure in unique(plotting.data$X_exposure)){
		index <- plotting.data$X_strata==strata & plotting.data$X_outcome==outcome & plotting.data$X_exposure==exposure
		if(sum(index)==0) next
		num.graphs <- num.graphs + 1
		to.plot.data[[num.graphs]] <- plotting.data[index,]

		if(to.plot.data[[num.graphs]]$X_type[1]=="categorical"){
			y.label <- outcome
			if(to.plot.data[[num.graphs]]$X_method[1]=="logit"){
				y.label <- paste(y.label," (%)",sep="")
				to.plot.data[[num.graphs]]$X_observed <- to.plot.data[[num.graphs]]$X_observed*100
				to.plot.data[[num.graphs]]$X_l95 <- to.plot.data[[num.graphs]]$X_l95*100
				to.plot.data[[num.graphs]]$X_u95 <- to.plot.data[[num.graphs]]$X_u95*100	
			}
			x.label <- exposure
			labels <- unique(to.plot.data[[num.graphs]]$X_c_exposure)
			if(length(unique(to.plot.data[[num.graphs]]$X_c_exposure))==2) labels <- c("No","Yes")
			
			label.pos <- min(to.plot.data[[num.graphs]]$X_l95)-(max(to.plot.data[[num.graphs]]$X_u95)-min(to.plot.data[[num.graphs]]$X_l95))*0.075
			x.lim <- c(min(to.plot.data[[num.graphs]]$X_c_exposure)-0.25,max(to.plot.data[[num.graphs]]$X_c_exposure)+0.25)
			y.lim <- c(label.pos,max(to.plot.data[[num.graphs]]$X_u95))
			
			sig.figs <- 1
			to.plot.data[[num.graphs]]$label <- paste(
				format(round(to.plot.data[[num.graphs]]$X_observed, sig.figs), sig.figs),
				" (",
				format(round(to.plot.data[[num.graphs]]$X_l95, sig.figs), sig.figs),
				", ",
				format(round(to.plot.data[[num.graphs]]$X_u95, sig.figs), sig.figs),
				")",sep="")
			q <- ggplot(data= to.plot.data[[num.graphs]],mapping=aes(x=X_c_exposure,y=X_observed,ymin=X_l95,ymax=X_u95))
			q <- q + geom_linerange(lwd=2*size.multiplier)
			q <- q + geom_point(size=4*size.multiplier)
			q <- q + geom_text(aes(label=label),y=label.pos,size=10*0.352777778*size.multiplier)
			q <- q + scale_y_continuous(y.label,lim=y.lim)
			q <- q + scale_x_continuous(x.label,breaks=unique(to.plot.data[[num.graphs]]$X_c_exposure),labels=labels,lim=x.lim)
			q <- q + theme(axis.ticks 		= element_line(colour="black"))
			q <- q + theme(panel.background=element_rect(colour="white",fill="white"))
			q <- q + theme(axis.line=element_line(colour="black"))
			q <- q + theme(strip.background = element_blank())
			q <- q + theme(panel.grid.major = element_blank())
			q <- q + theme(panel.grid.minor = element_blank())
			q <- q + theme(legend.key.size 	= unit(1.5,"lines"))
			q <- q + theme(legend.key = element_blank())
			q <- q + theme(axis.title.x 	= element_text(size=10*size.multiplier,vjust=0,colour="black"))
			q <- q + theme(axis.title.y		= element_text(size=10*size.multiplier,angle=90,vjust=0.25,colour="black"))
			q <- q + theme(axis.text.y		= element_text(size=10*size.multiplier,hjust=1,vjust=0.4,colour="black"))
			q <- q + theme(axis.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.y		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.text		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.title		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.position="right")
			q <- q + theme(plot.margin = unit(c(0.5,0.5,0.5,0.5),"lines"))
			q <- q + theme(plot.title = element_text(size=10*size.multiplier,hjust=0.5,vjust=1))
			to.plot.graphs[[num.graphs]] <- q + labs(title="")
			
		} else if(to.plot.data[[num.graphs]]$X_type[1]=="binned" | to.plot.data[[num.graphs]]$X_type[1]=="continuous_exposure"){
			y.label <- outcome
			if(to.plot.data[[num.graphs]]$X_type[1]=="binned"){
				y.label <- paste(y.label," among binned observations (%)",sep="")
				to.plot.data[[num.graphs]]$X_observed <- to.plot.data[[num.graphs]]$X_observed*100
				to.plot.data[[num.graphs]]$X_l95 <- to.plot.data[[num.graphs]]$X_l95*100
				to.plot.data[[num.graphs]]$X_u95 <- to.plot.data[[num.graphs]]$X_u95*100	
			}
			x.label <- exposure
			
			q <- ggplot(data= to.plot.data[[num.graphs]],mapping=aes(x=X_c_exposure,y=X_observed))
			q <- q + geom_point(size=2*size.multiplier)
			q <- q + stat_smooth(method="lm",se=FALSE,col="red",lwd=2*size.multiplier)
			q <- q + scale_y_continuous(y.label)
			q <- q + scale_x_continuous(x.label)
			q <- q + theme(axis.ticks 		= element_line(colour="black"))
			q <- q + theme(panel.background=element_rect(colour="white",fill="white"))
			q <- q + theme(axis.line=element_line(colour="black"))
			q <- q + theme(strip.background = element_blank())
			q <- q + theme(panel.grid.major = element_blank())
			q <- q + theme(panel.grid.minor = element_blank())
			q <- q + theme(legend.key.size 	= unit(1.5,"lines"))
			q <- q + theme(legend.key = element_blank())
			q <- q + theme(axis.title.x 	= element_text(size=10*size.multiplier,vjust=0,colour="black"))
			q <- q + theme(axis.title.y		= element_text(size=10*size.multiplier,angle=90,vjust=0.25,colour="black"))
			q <- q + theme(axis.text.y		= element_text(size=10*size.multiplier,hjust=1,vjust=0.4,colour="black"))
			q <- q + theme(axis.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.y		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.text		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.title		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.position="right")
			q <- q + theme(plot.margin = unit(c(0.5,0.5,0.5,0.5),"lines"))
			q <- q + theme(plot.title = element_text(size=10*size.multiplier,hjust=0.5,vjust=1))
			to.plot.graphs[[num.graphs]] <- q + labs(title="")

		} else if(to.plot.data[[num.graphs]]$X_type[1]=="logit_continuous_exposure"){
			y.label <- paste(outcome," (probability)",sep="")
			x.label <- exposure
			
			q <- ggplot(data= to.plot.data[[num.graphs]],mapping=aes(x=X_c_exposure,y=X_observed))
			q <- q + stat_smooth(method="glm",family="binomial",formula=y~x,se=FALSE,col="red",lwd=2*size.multiplier)
			#q <- q + geom_point()
			q <- q + scale_y_continuous(y.label)
			q <- q + scale_x_continuous(x.label)
			q <- q + theme(axis.ticks 		= element_line(colour="black"))
			q <- q + theme(panel.background=element_rect(colour="white",fill="white"))
			q <- q + theme(axis.line=element_line(colour="black"))
			q <- q + theme(strip.background = element_blank())
			q <- q + theme(panel.grid.major = element_blank())
			q <- q + theme(panel.grid.minor = element_blank())
			q <- q + theme(legend.key.size 	= unit(1.5,"lines"))
			q <- q + theme(legend.key = element_blank())
			q <- q + theme(axis.title.x 	= element_text(size=10*size.multiplier,vjust=0,colour="black"))
			q <- q + theme(axis.title.y		= element_text(size=10*size.multiplier,angle=90,vjust=0.25,colour="black"))
			q <- q + theme(axis.text.y		= element_text(size=10*size.multiplier,hjust=1,vjust=0.4,colour="black"))
			q <- q + theme(axis.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.y		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(strip.text.x		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.text		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.title		= element_text(size=10*size.multiplier,hjust=0.5,colour="black"))
			q <- q + theme(legend.position="right")
			q <- q + theme(plot.margin = unit(c(0.5,0.5,0.5,0.5),"lines"))
			q <- q + theme(plot.title = element_text(size=10*size.multiplier,hjust=0.5,vjust=1))
			to.plot.graphs[[num.graphs]] <- q + labs(title="")

		}
		if(incorporate_strata) to.plot.graphs[[num.graphs]] <- q + labs(title=strata)
	}	
	
	if(incorporate_strata){
		png(paste(figure,".png",sep=""),width=640*(num.graphs+0),height=480*1.5)
	} else png(paste(figure,".png",sep=""),width=640*1.5,height=480*1.5)		
	grid.newpage()
	pushViewport(viewport(layout = grid.layout(1, num.graphs, heights = unit(rep(1,num.graphs), "null"))))   
	for(i in 1:num.graphs) try(print(to.plot.graphs[[i]], vp = viewport(layout.pos.row = 1, layout.pos.col = i)),TRUE)
	dev.off()
}

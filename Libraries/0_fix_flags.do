* If on a mac, don't need an extra command
* If on windows, need CMD BATCH
if(trim(upper("$LOCATION_OF_R"))=="RSCRIPT"){
	global R_EXTRA_COMMAND=""
}
else {
	global R_EXTRA_COMMAND="CMD BATCH"
}

* If results directory left blank, insert default of "Results"
if(trim("$RESULTS_DIRECTORY")==""){
	global RESULTS_DIRECTORY="Results"
}


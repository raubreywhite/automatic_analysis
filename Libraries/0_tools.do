* Turns all empty/blank strings to missing
capture program drop make_clean
program make_clean
	foreach X of varlist * {
		capture quietly replace `X'="" if `X'==" "
		capture quietly replace `X'="" if `X'=="."
	}
end

* Adds one line to the bottom of the dataset
capture program drop add_one_line
program add_one_line
	quietly count
	local N=r(N)+1
	quietly set obs `N'
end

* Moves dataset down one line
capture program drop shuffle_down
program shuffle_down

	* Adds an extra row to dataset
	add_one_line
	
	* Copies the dataset down one line
	quietly count
	local N=r(N)
	forvalues X=`N'(-1)2 {
		foreach Y of varlist * {
			local Z=`X'-1
			quietly replace `Y'=`Y'[`Z'] in `X'
		}
	}
	* Clears the first line
	foreach Y of varlist * {
		capture quietly replace `Y'="" in 1
		capture quietly replace `Y'=. in 1
	}
end

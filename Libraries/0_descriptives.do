* Performs Pearson's Chi-Squared test for exposure: two differing strata, outcome: categorical variable
capture program drop test_cat
program test_cat, rclass
	syntax anything, strata(string) [move_ends(integer 1)]

	preserve

	* Extracts exposure
	local Y=1
	foreach X of local anything {
		if(`Y'==1){
			local exposure="`X'"
		}
		local Y=`Y'+1
	}
	local num_arguments=`Y'-1

	* Generates categories
	quietly gen _outcome=.
	local Y=1
	foreach X of local anything {
		if(`Y'==1){
			local Y=`Y'+1
			continue
		}
		if(mod(`Y',2)==0){
			local l=`X'
		}
		else {
			local u=`X'
			local num=(`Y'-1)/2
			
			if(`move_ends'==1){
				if(`Y'==3){
					quietly sum `exposure'
					local l=r(min)
				}
				if(`Y'==`num_arguments'){
					quietly sum `exposure'
					local u=r(max)
				}
			}
			
			quietly gen _xshort`num'=`exposure'
			quietly recode _xshort`num'(`l'/`u'=1)(-10000/10000=0)(.=0)
			quietly replace _outcome=`num'-1 if _xshort`num'==1
		}
		local Y=`Y'+1
	}
	
	* Generates exposure (i.e. strata)
	quietly gen _exposure=.
	local Y=0
	foreach X of local strata {
		quietly replace _exposure=`Y' if `X'
		local Y=`Y'+1
	}
	
	* Performs Pearson's Chi-Squared test
	quietly tab _outcome _exposure, chi
	return scalar pvalue =r(p)

	restore
end

* Turns a variable into categories and then summarises it
capture program drop summarise_cat
program summarise_cat, rclass
	syntax anything, outcome(string) [move_ends(integer 0) dp(integer 1)]

	preserve

	quietly gen level_lower =.
	quietly gen level_upper =.
	quietly gen n=.
	quietly gen est =.
	quietly gen est_nomiss=.
	
	* Extracts exposure
	local Y=1
	foreach X of local anything {
		if(`Y'==1){
			local exposure="`X'"
		}
		local Y=`Y'+1
	}
	local num_arguments=`Y'-1

	* Generates categories
	local Y=1
	foreach X of local anything {
		if(`Y'==1){
			local Y=`Y'+1
			continue
		}
		if(mod(`Y',2)==0){
			local l=`X'
		}
		else {
			local u=`X'
			local num=(`Y'-1)/2
			
			if(`move_ends'==1){
				if(`Y'==3){
					quietly sum `exposure'
					local l=r(min)
				}
				if(`Y'==`num_arguments'){
					quietly sum `exposure'
					local u=r(max)
				}
			}
			
			quietly gen _xshort`num'=`exposure'
			quietly recode _xshort`num'(`l'/`u'=1)(-10000/10000=0)
			
			quietly count if _xshort`num'==1
			quietly replace n=r(N) in `num'
			
			quietly sum _xshort`num'
			quietly replace est_nomiss=r(mean)*100 in `num'

			quietly recode _xshort`num'(.=0)
			quietly sum _xshort`num'
			quietly replace est=r(mean)*100 in `num'

			quietly replace level_lower=`l' in `num'
			quietly replace level_upper=`u' in `num'
		}
		local Y=`Y'+1
	}
	
	local num=`num'+1
	quietly gen _xshort`num'miss=`exposure'
	quietly recode _xshort`num'miss (-10000/10000=0)(.=1)
	
	quietly count if _xshort`num'miss==1
	quietly replace n=r(N) in `num'
			
	quietly sum _xshort`num'miss
	quietly replace est=r(mean)*100 in `num'
				
	quietly keep level_lower-est est_nomiss
	quietly drop if missing(est)

	* Formats into strings with appropriate decimal places
	quietly tostring n, replace format("%12.0f")  force
	quietly tostring est, replace format("%12.1f")  force
	quietly tostring est_nomiss, replace format("%12.1f")  force
	quietly replace est=est+" ("+est_nomiss+")" if est_nomiss!="."
	quietly drop est_nomiss

	* Formats the categories' levels
	quietly gen trimmed_level_lower=trunc(level_lower*(10^`dp'))/(10^`dp')
	quietly gen trimmed_level_upper=trunc(level_upper*(10^`dp'))/(10^`dp')
	quietly tostring trimmed_level_lower, replace format("%12.0f")  force
	quietly tostring trimmed_level_upper, replace format("%12.0f")  force

	quietly replace level_lower=trunc(level_lower*(10^`dp'))/(10^`dp')
	quietly replace level_upper=trunc(level_upper*(10^`dp'))/(10^`dp')
	
	quietly tostring level_lower, replace format("%12.`dp'f")  force
	quietly tostring level_upper, replace format("%12.`dp'f")  force
	
	quietly gen level=level_lower+" to "+level_upper
	
	quietly replace level="No" if trimmed_level_lower=="0" & trimmed_level_upper=="0"
	quietly replace level="Yes" if trimmed_level_lower=="1" & trimmed_level_upper=="1"
	quietly replace level="Missing" if level==". to ."
	quietly replace level="1" if level=="Yes" & level[_n+1]!="Missing"
	quietly replace level="0" if level=="No" & level[_n+1]!="Yes"

	quietly keep level n est
	quietly order level

	shuffle_down
		
	quietly gen outcome="`outcome'" in 1
	quietly order outcome
	quietly count
	local N=r(N)+1
	quietly set obs `N'
	quietly save "_summary.dta", replace
	
	restore
end

* Turns a variable into categories and then summarises it, allowing for multiple strata
capture program drop summarise_cat_stratified
program summarise_cat_stratified, rclass
	syntax anything, outcome(string) strata(string) strata_labels(string) [move_ends(integer 0) dp(integer 1)]
	preserve
	
	tempfile __temp1 __temp2
	quietly save `__temp1'
	local Y=1
	foreach X of local strata {
		quietly use `__temp1', clear
		
		local pvalue=100
		if(`Y'>=2 & mod(`Y',2)==0){
			local strata1="`X'"
		}
		else if(`Y'>=2 & mod(`Y',2)==1){
			test_cat `anything', strata("`strata1' `X'")
			local pvalue=r(pvalue)
		}
		
		quietly keep if `X'
		summarise_cat `anything', outcome("`outcome'") move_ends(`move_ends') dp(`dp')
		
		quietly use "_summary.dta", clear
		quietly gen name`Y'=""
		quietly order outcome level name n est
		quietly gen id=_n
		quietly ren outcome outcome`Y'
		quietly ren level level`Y'
		quietly ren n n`Y'
		quietly ren est est`Y'
		quietly gen pvalue`Y'=.
		quietly count
		local N=r(N)-1
		quietly replace pvalue`Y'=`pvalue' in `N'
		quietly tostring pvalue`Y', replace format("%12.3f") force
		quietly replace pvalue`Y'="<0.001" if pvalue`Y'=="0.000"
		quietly replace pvalue`Y'="" if pvalue`Y'=="."
		
		if(`Y'!=1){
			quietly save `__temp2', replace
			quietly use "_final_summary.dta", clear
			quietly joinby id using "`__temp2'", unm(b)
			quietly drop _merge outcome`Y' level`Y'
		}
		quietly save "_final_summary.dta", replace
		
		local Y=`Y'+1
	}

	quietly drop id
	
	* Formatting
	quietly label var outcome1 "Outcome"
	quietly label var level1 "Level"
	
	local Y=1
	foreach X of local strata_labels {
		quietly local blah=subinstr("`X'","."," ",.)
		quietly label var name`Y' "`blah'"
		quietly label var n`Y' "N"
		quietly label var est`Y' "Perc"
		quietly label var pvalue`Y' "Pvalue"
		quietly local Y=`Y'+1
	}
	quietly save "_summary.dta", replace
	quietly erase "_final_summary.dta"
	restore
end

* Takes input from excel file and generates descriptive tables
capture program drop summarise_from_excel
program summarise_from_excel, rclass
	syntax [anything] using/
	
	* Checks to make sure all files are where they should be
	quietly capture confirm file "Results"
	if(_rc!=0){
		quietly mkdir "Results"
	}
	if("`anything'"==""){
		local anything="Analyses/1_descriptives.xlsx"
	}
	
	quietly capture confirm file "`anything'"
	if(_rc!=0){
		error 601
	}
	quietly capture confirm file "`using'"
	if(_rc!=0){
		error 601
	}
	
	* Erase files that shouldn't be there
	local myfilelist: dir "." files "_*.dta"
	foreach filename of local myfilelist {
		capture erase "`filename'"
	}

	* Will this descriptive table require footnotes?
	quietly use "`using'", clear	
	quietly import excel using "`anything'", clear firstrow
	quietly keep footnotes
	quietly drop if missing(footnotes)
	quietly ren footnotes A
	tempfile footnotes
	quietly save `footnotes'
	local use_footnotes=0
	quietly count
	local N=r(N)
	if(`N'>0){
		local use_footnotes=1
	}
	
	* Extracting flags from excel punchcard
	tempfile final_summary_file_table1
	quietly import excel using "`anything'", clear firstrow
	
	local file_name=file_name[1]
	local title=title[1]
	
	local temp=testing[1]
	local testing=0
	if("`temp'"=="yes"){
		local testing=1
	}
	
	* Extracting strata variables and corresponding strata labels
	quietly count if !missing(strata_variables)
	local N=r(N)
	local strata=""
	local strata_labels=""
	forvalues X=1/`N' {
		local temp=strata_variables[`X']
		local strata="`strata' `temp'"
		
		local temp=subinstr(strata_names[`X']," ",".",.)
		local strata_labels="`strata_labels' `temp'"
	}
	
	* Summarizing for all requested descriptive variables
	quietly count if !missing(descriptive_variables)
	local N=r(N)
	forvalues X=1/`N' {
		di "`X'/`N'"
		preserve
		local descriptive_variables=descriptive_variables[`X']
		local descriptive_names=descriptive_names[`X']
		local categorisation=categorisation[`X']
		local categorisation_decimal_places=categorisation_decimal_places[`X']	
		quietly use "`using'", clear	
		summarise_cat_stratified `descriptive_variables' `categorisation', outcome("`descriptive_names'") move_ends(1) dp(`categorisation_decimal_places') strata("`strata'") strata_labels("`strata_labels'")

		quietly use "_summary.dta", clear
		if(`X'!=1){
			quietly use `final_summary_file_table1', clear
			quietly append using "_summary.dta"
		}
		quietly save `final_summary_file_table1', replace
		restore
	}
	quietly use `final_summary_file_table1', clear
	quietly save "_summary.dta", replace
	quietly erase "_summary.dta"
	
	* Making the table headings look nice
	quietly export excel using "Results/`file_name'", replace firstrow(varlabels)
	quietly import excel using "Results/`file_name'", clear
	shuffle_down
	
	local Y=1
	foreach X of varlist * {
		local name=`X'[2]
		if(`Y'<=2){
			quietly replace `X'="`name'" in 1
			quietly replace `X'="" in 2
		}
		else if(`Y'>2 & mod(`Y',4)==3){
			quietly replace `X'="" in 2
			local stored_name="`name'"
		}
		else if(`Y'>2 & (mod(`Y',4)==0 | mod(`Y',4)==0)){
			quietly replace `X'="`stored_name'" in 1
		}
		else if(`Y'>2 & mod(`Y',4)==2 & mod(`Y',12)!=2 & `testing'==1){
			quietly drop `X'
		}
		else if(`Y'>2 & mod(`Y',4)==2 & `testing'==0){
			quietly drop `X'
		}
		
		local Y=`Y'+1
	}
	
	* Adding the table title
	shuffle_down
	shuffle_down
	quietly replace A="`title'" in 1
	
	* Adding the table footnotes
	if(`use_footnotes'==1){
		quietly append using `footnotes'
	}
	
	* Final table produced
	quietly make_clean
	quietly export excel using "Results/`file_name'", replace miss("")
		
end

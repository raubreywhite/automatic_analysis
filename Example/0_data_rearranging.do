*display _newline(200)


clear
clear matrix
clear mata
set maxvar 20000

set more off, perm

if("`WORKING_DIRECTORY'"!=""){
	cd "`WORKING_DIRECTORY'"
}
else {
	cd "/Volumes/Research/Code/MOBA breastfeeding/"
}
use "/Volumes/Research/Data/MOBA Masterfile/edited_cleaned_missing_anthro.dta", clear

replace gestational_age=gestational_age/7
gen bf_cat=has_breast
recode bf_cat (15/100=5)(12/100=4)(6/12=3)(3/6=2)(1/3=1)(0=0)
tab bf_cat has_food_allergy
replace has_food_allergy=. if has_skjema_5==0
tab has_food_allergy_18mnth has_conf
tab has_food_allergy_18mnth 
tab has_conf

recode has_csec_moba(2=1)

*keep if maternal_parity_complicated==0 | maternal_parity_complicated==1

logit has_food_allergy maternal_parity_complicated

save "Data/analysis_file.dta", replace


* SET LOCATIONS REGARDING AUTOMATED ANALYSES [YOU CAN EDIT THIS SECTION]
global PROGRAM_DIRECTORY="/Users/raw996/Dropbox/Code/automatic_analysis/"
global LOCATION_OF_R="RScript"

* SET LOCATIONS REGARDING WHERE YOUR ANALYSIS WILL BE RUN [YOU CAN EDIT THIS SECTION]
global WORKING_DIRECTORY="/Volumes/Research/Code/MOBA breastfeeding/"
global RESULTS_DIRECTORY="Results"


*************************
*************************
*** DO NOT EDIT BELOW ***
*************************
*************************


*************************
* CLEAR MEMORY
clear
clear matrix
clear mata
set maxvar 20000
set more off, perm

*************************
* LOAD PACKAGES
run "$PROGRAM_DIRECTORY/automatic_analysis.do"
cd "$WORKING_DIRECTORY"

*************************
* FORMAT DATA
run "0_data_rearranging.do"

*************************
* RUN ANALYSIS
run "1_analysis.do"

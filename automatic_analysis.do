local saved_wd="`c(pwd)'"

if("$PROGRAM_DIRECTORY"==""){
	quietly cd "~/Dropbox/Code/automatic_analysis"
}
else {
	quietly cd "$PROGRAM_DIRECTORY"
}

run "Libraries/0_fix_flags.do"
run "Libraries/0_tools.do"
run "Libraries/0_descriptives.do"
run "Libraries/0_analyses.do"

quietly cd "`saved_wd'"

